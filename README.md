# Utiliser la Forge avec VSCodium ou Visual Studio Code

## Installer Git
Voir [Git](https://git-scm.com/download)

## Installer VSCodium ou Visual Studio Code
Voir [VSCodium](https://vscodium.com) ou [Visual Studio Code](https://code.visualstudio.com/)

## Ajouter l'extension Git Worflow
* Ouvrir VSCodium ou Visual Studio Code
* Aller dans "Extensions"
* Recherche "GitLab Workflow", l'installer et l'activer

## Lier la Forge à VSCodium ou Visual Studio avec Git Workflow
Git Worflow nécessite de créer un token d'accès personnel GitLab et de l'assigner à l'extension.

### Créer un jeton
* Se connecter à la Forge
* Aller dans "Jetons d'accès personnels": https://forge.apps.education.fr/-/user_settings/personal_access_tokens
* Cliquer sur le bouton "Ajouter un nouveau jeton"
* Donner un nom au jeton
* Définir une date d'expiration du jeton
* Cocher "api" et "read_user" et valider
* Sur la page suivante: afficher le jeton et copier/sauvegarder le jeton. ATTENTION: pour des raisons de sécurité, cette valeur n'est jamais affichée à nouveau.

### Ajouter un jeton à Git Workflow
* Ouvrir VSCodium ou Visual Studio Code, puis ouvrir la palette de commandes en appuyant sur `Ctrl` + `Maj` + `P`.
* Dans la palette de commandes, chercher "GitLab : Ajouter un compte" et appuyez sur `Entrée`.
* Dans l'URL de l'instance GitLab, coller l'URL complète de la Forge: https://forge.apps.education.fr
* Coller le token d'accès personnel GitLab et appuyer sur `Entrée`.

Git Workflow est maintenant prêt à l'emploi.

## Cloner un dépôt
### Dépots personnels
* Appuyer sur `Ctrl` + `Maj` + `P`
* Choisir "Git: Clone"
* Choisir "Clone from GitLab (https://forge.apps.education.fr - xxxx)"
* Sélectionner le dépôt à cloner

### Dépôts (personnels ou autres) avec URL
* Appuyer sur `Ctrl` + `Maj` + `P`
* Choisir "Git: Clone"
* Coller l'adresse du dépôt à cloner (par exemple: https://forge.apps.education.fr/laurent.abbal/utiliser-la-forge-avec-vscodium-ou-visual-studio-code ou https://forge.apps.education.fr/laurent.abbal/utiliser-la-forge-avec-vscodium-ou-visual-studio-code.git)
